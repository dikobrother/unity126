using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class MusicController : MonoBehaviour
{
    [SerializeField] private AudioMixer _audioMixer;
    [SerializeField] private string _offMusic;
    [SerializeField] private string _onMusic;
    [SerializeField] private string _offSound;
    [SerializeField] private string _onSound;
    [SerializeField] private TMP_Text _soundText;
    [SerializeField] private TMP_Text _musicText;
    [SerializeField] private Button _musicButton;
    [SerializeField] private Button _soundButton;

    private float _minVolume = -80f;
    private float _maxVolume = 0f;

    private void Start()
    {
        if (!PlayerPrefs.HasKey("Music") || !PlayerPrefs.HasKey("Sound"))
        {
            PlayerPrefs.SetInt("Music", 1);
            PlayerPrefs.SetInt("Sound", 1);
        }
        UpdateAudio();
    }

    public void UpdateAudio()
    {
        
        if (PlayerPrefs.GetInt("Music") == 1)
        {
            TurnOnMusic();
        }
        else
        {
            TurnOffMusic();
        }
        if (PlayerPrefs.GetInt("Sound") == 1)
        {
            TurnOnSound();
        }
        else
        {
            TurnOffSound();
        }

    }

    private void TurnOffSound()
    {
        _audioMixer.SetFloat("SoundVolume", _minVolume);
        PlayerPrefs.SetInt("Sound", 0);
        _soundText.text = _offSound;
        _soundButton.onClick.RemoveAllListeners();
        _soundButton.onClick.AddListener(TurnOnSound);
    }

    private void TurnOnSound()
    {
        _audioMixer.SetFloat("SoundVolume", _maxVolume);
        PlayerPrefs.SetInt("Sound", 1);
        _soundText.text = _onSound;
        _soundButton.onClick.RemoveAllListeners();
        _soundButton.onClick.AddListener(TurnOffSound);
    }

    public void TurnOffMusic()
    {
        _audioMixer.SetFloat("MusicVolume", _minVolume);
        PlayerPrefs.SetInt("Music", 0);
        _musicText.text = _offMusic;
        _musicButton.onClick.RemoveAllListeners();
        _musicButton.onClick.AddListener(TurnOnMusic);
    }

    public void TurnOnMusic()
    {
        _audioMixer.SetFloat("MusicVolume", _maxVolume);
        PlayerPrefs.SetInt("Music", 1);
        _musicText.text = _onMusic;
        _musicButton.onClick.RemoveAllListeners();
        _musicButton.onClick.AddListener(TurnOffMusic);
    }


}
