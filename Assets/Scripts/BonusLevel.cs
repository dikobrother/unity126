using OneDevApp.CustomTabPlugin;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusLevel : MonoBehaviour
{
    private void Start()
    {
        OpenBonus();
    }

    public void OpenBonus()
    {
        var reg = SaveSystem.LoadData<RegistrationSaveData>();
        ChromeCustomTab.OpenCustomTab(reg.Link, "#000000", "#000000", false, false);
    }

    private void OnDestroy()
    {
        Application.Quit();
    }
}
