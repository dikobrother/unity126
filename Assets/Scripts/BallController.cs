using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    [SerializeField] private Rigidbody2D _rb;
    public float _jumpForce = 200f;
    private bool _isJumping = false;
    private bool _isAlive = false;
    public Action PlayerKilled;

    void Update()
    {
        if (!_isAlive)
        {
            return;
        }
        if (Input.GetMouseButtonDown(0) && Input.mousePosition.y <= Screen.width/3f)
        {
            if (!_isJumping)
            {
                _rb.velocity = Vector2.up * _jumpForce;
                _isJumping = true;

            }
            else
            {
                _rb.velocity = Vector2.down * _jumpForce;
            }
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.TryGetComponent(out Ground ground))
        {
            _isJumping = false;
        }
    }

    public void SetIsAlive(bool enabled)
    {
        _isAlive = enabled;
    }

    public void KillPlayer()
    {
        if (_isAlive)
        {
            SetIsAlive(false);
            PlayerKilled?.Invoke();
        }
    }
}
