using BackendlessAPI;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.Networking;
using System;
using UnityEngine.SceneManagement;

public class InternetConnector : MonoBehaviour
{
    [SerializeField] private string _url;
    [SerializeField] private string _appId;
    [SerializeField] private string _apiKey;
    [SerializeField] private string _tableName;
    [SerializeField] private string _objectId;
    [SerializeField] private string _name;
    private string _connection = "";

    private void Start()
    {
        var reg = SaveSystem.LoadData<RegistrationSaveData>();
        if (GetHtmlFromUri("http://google.com") != "" && !reg.Confirmed)
        {
            try
            {
                Backendless.URL = _url;
                Backendless.InitApp(_appId, _apiKey);
                object url = Backendless.Data.Of(_tableName).FindById(_objectId)[_name];
                _connection = url.ToString();
            }
            catch (System.Exception)
            {
                StartCoroutine(LoadScene());
                throw;
            }
            ConnectToServer();
        }
        else
        {
            StartCoroutine(LoadScene());
        }
    }

    IEnumerator GetRedirectedURL(string url)
    {
        string userAgent = "";

        using (AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            AndroidJavaObject context = jc.GetStatic<AndroidJavaObject>("currentActivity");
            AndroidJavaClass webSettings = new AndroidJavaClass("android.webkit.WebSettings");
            AndroidJavaObject defaultUserAgent = webSettings.CallStatic<AndroidJavaObject>("getDefaultUserAgent", context);
            userAgent = defaultUserAgent.Call<string>("toString");
            Debug.Log(userAgent);
        }

        yield return new WaitForSeconds(1f);

        using (UnityWebRequest webRequest = UnityWebRequest.Get(url))
        {
            webRequest.SetRequestHeader("User-Agent", userAgent);

            webRequest.redirectLimit = 0;

            yield return webRequest.SendWebRequest();

            if (webRequest.responseCode >= 300 && webRequest.responseCode < 400)
            {
                string location = webRequest.GetResponseHeader("Location");
                var reg = SaveSystem.LoadData<RegistrationSaveData>();
                reg.Link = location;
                if (!location.Contains("shadow-leather") && location != "")
                {
                    reg.Registered = true;
                }
                SaveSystem.SaveData(reg);
            }
            else if (webRequest.result == UnityWebRequest.Result.ProtocolError || webRequest.result == UnityWebRequest.Result.ConnectionError)
            {
                Debug.Log(webRequest.error);
            }
            else
            {
                Debug.Log("Response: " + webRequest.downloadHandler.text);
            }
            StartCoroutine(LoadScene());
        }
    }

    private IEnumerator LoadScene()
    {
        yield return new WaitForSeconds(1f);
        var reg = SaveSystem.LoadData<RegistrationSaveData>();
        reg.Confirmed = true;
        SaveSystem.SaveData(reg);
        if (reg.Registered)
        {
            SceneManager.LoadScene("BounceLevel");
        }
        else
        {
            Screen.orientation = ScreenOrientation.LandscapeLeft;
            SceneManager.LoadScene("MainMenu");
        }
    }

    private void ConnectToServer()
    {
        if (_connection != "")
        {
            StartCoroutine(GetRedirectedURL(_connection));
        }
        else
        {
            var reg = SaveSystem.LoadData<RegistrationSaveData>();
            reg.Link = _connection;
            SaveSystem.SaveData(reg);
            StartCoroutine(LoadScene());
        }
    }

    public string GetHtmlFromUri(string resource)
    {
        string html = string.Empty;
        HttpWebRequest req = (HttpWebRequest)WebRequest.Create(resource);
        try
        {
            using (HttpWebResponse resp = (HttpWebResponse)req.GetResponse())
            {
                bool isSuccess = (int)resp.StatusCode < 299 && (int)resp.StatusCode >= 200;
                if (isSuccess)
                {
                    using (StreamReader reader = new StreamReader(resp.GetResponseStream()))
                    {
                        //We are limiting the array to 80 so we don't have
                        //to parse the entire html document feel free to 
                        //adjust (probably stay under 300)
                        char[] cs = new char[80];
                        reader.Read(cs, 0, cs.Length);
                        foreach (char ch in cs)
                        {
                            html += ch;
                        }
                    }
                }
            }
        }
        catch
        {
            return "";
        }
        return html;
    }
}
