using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverWindow : MonoBehaviour
{
    [SerializeField] private GameObject _mainPanel;
    [SerializeField] private GameObject _recordPanel;
    [SerializeField] private TMP_Text _currentRecordText;
    [SerializeField] private TMP_Text _maxRecordText;
    [SerializeField] private TMP_Text _mainRecordText;

    private void OnEnable()
    {
        ShowMainPanel();
    }

    public void SetCurrentRecord(int record)
    {
        _currentRecordText.text = "Last record: " + record;
        _mainRecordText.text = record.ToString();
        _maxRecordText.text = "Max record: " + SaveSystem.LoadData<GamesSaveData>().Score;
    }

    public void ShowRecordPanel()
    {
        _recordPanel.SetActive(true);
        _mainPanel.SetActive(false);
    }

    public void ShowMainPanel()
    {
        _recordPanel.SetActive(false);
        _mainPanel.SetActive(true);
    }

    public void RestartGame()
    {
        SceneManager.LoadScene("GameScene");
        Time.timeScale = 1;
    }

    public void Exit()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("MainMenu");
    }
}
