using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfinityLevelGenerator : MonoBehaviour
{
    [SerializeField] private GameObject _firstPlatform;
    [SerializeField] private List<GameObject> _prefabCollection;
    [SerializeField] private float _moveSpeed;
    [SerializeField] private float _destroyXPosition;
    [SerializeField] private int _maxPlatforms = 3;
    private bool _isGameOver = true;
    private List<GameObject> _activePlatforms = new List<GameObject>();

    private void Start()
    {
        _activePlatforms.Add(_firstPlatform);
        for (int i = 0; i < _maxPlatforms; i++)
        {
            SpawnPlatform();
        }
    }

    void Update()
    {
        if (_isGameOver)
        {
            return;
        }
        MovePlatforms();
    }

    public void SetGameOver(bool enable)
    {
        _isGameOver = enable;
    }

    void SpawnPlatform()
    {
        int randomIndex = Random.Range(0, _prefabCollection.Count);
        GameObject newPlatform = Instantiate(_prefabCollection[randomIndex], _activePlatforms[_activePlatforms.Count - 1].transform.position + new Vector3(25f, 0f, 0f), Quaternion.identity);
        _activePlatforms.Add(newPlatform);
    }

    void MovePlatforms()
    {
        foreach (var platform in _activePlatforms)
        {
            platform.transform.position += Vector3.left * _moveSpeed * Time.deltaTime;
        }
    }

    public void CheckDestroyPlatforms()
    {
            Destroy(_activePlatforms[0]);
            _activePlatforms.RemoveAt(0);
            SpawnPlatform();
    }
}
