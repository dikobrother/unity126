using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private TMP_Text _scoreText;
    [SerializeField] private GameObject _records;
    [SerializeField] private GameObject _settings;
    [SerializeField] private GameObject _main;

    private void Awake()
    {
        _scoreText.text = "Max score: " + SaveSystem.LoadData<GamesSaveData>().Score;
        ShowMain();
    }

    public void ShowRecords()
    {
        _main.SetActive(false);
        _settings.SetActive(false);
        _records.SetActive(true);
    }

    public void ShowMain()
    {
        _main.SetActive(true);
        _records.SetActive(false);
        _settings.SetActive(false);
    }

    public void ShowSettings()
    {
        _main.SetActive(false);
        _records.SetActive(false);
        _settings.SetActive(true);
    }

    public void StartGame()
    {
        SceneManager.LoadScene("GameScene");
    }
}
