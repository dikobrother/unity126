using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Settings : MonoBehaviour
{
    [SerializeField] private GameObject _mainPanel;
    [SerializeField] private GameObject _settingsPanel;

    private void OnEnable()
    {      
        ShowMainPanel();
        _mainPanel.transform.DOScale(1, 0.3f).SetEase(Ease.OutBounce).SetLink(gameObject);
    }

    public void ShowMainPanel()
    {
        _mainPanel.SetActive(true);
        _settingsPanel.SetActive(false);
    }

    public void ShowSettingsPanel()
    {
        _settingsPanel.SetActive(true);
        _mainPanel.SetActive(false);
    }
}
