using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class GamesSaveData : SaveData
{
    public int Score;

    public GamesSaveData(int score)
    {
        Score = score;
    }
}
