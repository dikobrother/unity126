using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [SerializeField] private BallController _ballController;
    [SerializeField] private GameOverWindow _gameOverWindow;
    [SerializeField] private InfinityLevelGenerator _infinityLevelGenerator;
    [SerializeField] private TMP_Text _timerText;
    [SerializeField] private TMP_Text _scoreText;
    [SerializeField] private GameObject _loseScreen;
    [SerializeField] private GameObject _settingsScreen;
    [SerializeField] private GameObject _settingsButton;
    [SerializeField] private AudioSource _loseSFX;
    private IEnumerator _scoreCounter;
    private int _startPrepareSeconds = 3;
    private int _score;

    private void Start()
    {
        StartGame();
    }

    private void StartGame()
    {
        _settingsButton.SetActive(false);
        _score = 0;
        _scoreText.text = _score.ToString();
        _timerText.text = _startPrepareSeconds.ToString();
        Sequence mySequence = DOTween.Sequence();
        mySequence
            .Append(_timerText.transform.DOScale(1, 0.8f))
            .Append(_timerText.transform.DOScale(0, 0.2f));
        mySequence.SetLink(gameObject).SetLoops(3, LoopType.Restart).OnStepComplete(() =>
        {
            _startPrepareSeconds--;
            _timerText.text = _startPrepareSeconds.ToString();
        }).OnComplete(() =>
        {
            _infinityLevelGenerator.SetGameOver(false);
            _ballController.SetIsAlive(true);
            _scoreCounter = ScoreCounter();
            StartCoroutine(_scoreCounter);
            _settingsButton.SetActive(true);
        });
        mySequence.Play();
    }

    private IEnumerator ScoreCounter()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.5f);
            _score++;
            _scoreText.text = _score.ToString();
        }
    }

    public void PauseGame()
    {
        Time.timeScale = 0;
        _ballController.SetIsAlive(false);
        StopCoroutine(_scoreCounter);
        _settingsScreen.SetActive(true);
    }

    public void UnpauseGame()
    {
        Time.timeScale = 1;
        _ballController.SetIsAlive(true);
        _scoreCounter = ScoreCounter();
        StartCoroutine(_scoreCounter);
        _settingsScreen.SetActive(false);
    }

    private void OnEnable()
    {
        _ballController.PlayerKilled += EndGame;
    }

    private void OnDisable()
    {
        _ballController.PlayerKilled -= EndGame;
    }

    private void EndGame()
    {
        StopCoroutine(_scoreCounter);
        _loseSFX.Play();
        _settingsButton.SetActive(false);
        _ballController.SetIsAlive(false);
        _infinityLevelGenerator.SetGameOver(true);
        var games = SaveSystem.LoadData<GamesSaveData>();
        if (games.Score < _score)
        {
            games.Score = _score;
            SaveSystem.SaveData(games);
        }
        StartCoroutine(EndGameCoroutine());
    }

    private IEnumerator EndGameCoroutine()
    {
        yield return new WaitForSeconds(1f);
        _gameOverWindow.SetCurrentRecord(_score);
        _loseScreen.SetActive(true);
    }
}
