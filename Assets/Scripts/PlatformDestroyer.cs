using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformDestroyer : MonoBehaviour
{
    [SerializeField] private InfinityLevelGenerator _infinityLevelGenerator;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.TryGetComponent(out Platform platform))
        {
            _infinityLevelGenerator.CheckDestroyPlatforms();
        }
    }
}
